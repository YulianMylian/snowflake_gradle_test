package com.imprivata.astra;

import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = StartApplication.class)
public class BaseTest {
}