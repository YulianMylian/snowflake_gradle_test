package com.imprivata.astra.core.snowflake.dao.landing;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Primary
@Repository
@RequiredArgsConstructor
public class AuthDomainEventsDaoImpl implements AuthDomainEventsDao {

    @Autowired
    private final JdbcTemplate jdbcTemplate;

    @Override
    public int getAuthDomainEventsRecordsCount() {
        return jdbcTemplate.queryForObject("SELECT count(*) from AUTHN_DOMAIN_EVENTS", Integer.class);
    }
}