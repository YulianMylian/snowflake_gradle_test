package com.imprivata.astra.core.snowflake.dto.landing;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class AuthDomainEvents {

    private String recordMetaData;

    private String recordContent;
}