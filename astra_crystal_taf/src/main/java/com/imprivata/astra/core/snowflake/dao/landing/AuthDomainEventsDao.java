package com.imprivata.astra.core.snowflake.dao.landing;

import org.springframework.stereotype.Repository;

@Repository
public interface AuthDomainEventsDao {

    int getAuthDomainEventsRecordsCount();
}